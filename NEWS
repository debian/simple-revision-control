= src project news =

1.30: 2022-01-21::
   Now requires python3, since Ubuntu broke the "python" link.
   Fixed a bug in parsing of diff command with --.

1.29: 2021-08-20:
   Resynch fast-export behavior with git - no more branch-tip resets.
   Filenames with embdedded spaces are now processed correctly.
   Ubuntu has abolished /usr/bin/python, change shebang to python3.

1.28: 2020-02-03::
   Restore terminal-size computation inside Emacs.

1.27: 2019-09-05::
   Port fix for Solaris, which lacks a size operation in stty.
   Cope with the deprecation of the cgi module in Python 3.

1.26: 2019-05-12::
   Change name of "lift" command to "srcify".
   Proper inline documentation for srcify.
   Repair make check; do setup to perform validation before shipping.

1.25: 2019-05-10::
   New "lift" command for changing RCS and SCCS directories to be SRC-managed.

1.24: 2019-02-07::
   More fast-export bug fixes - make multifile export work properly.

1.23: 2019-02-06::
   Repair a serious bug in src fast-export

1.22: 2018-12-06::
   Correct behavior verified with OS X (which provides RCS 5.9.4).
   Correct behavior verified with Amazon Linux (which provides RCS 5.7).
   Correct behavior verified under OpenBSD, which has its own RCS port.
   The SCCS back end now supports tagging.
   To avoid errors, src will bail out if both .src and RCS directories exist.

1.21: 2018-11-27::
   Now tested with Schilling fork of SCCS. One minor bugfix in SCCS back end.

1.20: 2018-10-28::
   Add platform to "src version" output to make bug reports easier to generate.
   Signal-harden backend command execution.

1.19: 2018-10-27::
   Filenames containing embedded spaces are handled properly.
   The '--' token is interpreted correctly in cat commands.

1.18: 2018-03-17::
   Fix out-of-order 'revno' assignment when commits share same date.
   'list' & 'log' suppress RFC 822 headers inserted by 'fast-import'.
   'log' gained '-v' verbosity levels to show imported RFC 822 headers.
   'list' & 'log' show author date if in RFC 822 headers rather than committer.
   Improved 'fast-import/export' round-trip fidelity using RFC 822 headers.

1.17: 2017-11-14::
   Show diff of changes to be committed when editing/amending commit comment.
   'src commit' no longer opens editor when file has not been changed.
   'src diff' and 'src log' accept '-b' & '-w' to ignore whitespace changes.
   'git log -p 1' shows "big bang" creation diff (previously no diff for r1).
   Commands now operate on all managed files (as documented) when none given.
   Colored output no longer crashes under Python 3.
   'src log -<n>' is alias for '-l <n>' a la 'git log -<n>'; ditto 'src list'.

1.16: 2017-11-05::
   Output of src diff & src log are colorized a la git when issuing to terminal.
   'src log' now accepts '-p' to emit patches a la 'git log -p'.

1.15: 2017-10-30::
   Fixes for fast-import, fast-export, and exec-bit propagation.

1.14: 2017-10-17::
   Slightly improved boilerplate commit message.
   Fix for a minor command-parsing bug.

1.13: 2017-03-26::
   Improvement to tempfile handling; avoid wiring in /tmp.

1.12: 2017-01-24::
   Pylint cleanup, minor documentation fixes, and Python 3 port tweaks.

1.11: 2016-02-23::
   Now handles binary repository data cleanly under Python 3.
   Version command reports versions for the underlying Python and back end.

1.10: 2016-02-18::
   Code now runs under either Python 2 or Python 3.
   Restore (undocumented) add command for Emacs VC compatibility.

1.9: 2016-02-15::
   Fix status-computation bug introduced in 1.8.
   SCCS parsing can no longer be fooled by comment lines resembing delta headers.

1.8: 2016-02-14::
   Stamp files are no longer used; all SRC state is carried in the master itself.

1.7: 2016-02-10::
   New 'visualize' command makes a DOT/graphviz visualization of repo structure.
   It is now possible to range-restrict a tag or branch listing.

1.6: 2016-02-08::
   Improved prs log parsing in SCCS back end allows blank lines in comments.
   SCCS regression tests now really work (previously a false positive).

1.5: 2016-02-07::
   Bugfixes for SCCS support. It now has its own regression test.
   Documentation and FAQ update for the new back end.

1.4: 2016-02-05::
   Basic SCCS support - no branches or tags, limited diffs.
   The diff command no longer leaks native commit IDs onto the screen.
   In fast-export, properly sanitize RCS branch names illegal for git.

1.3: 2016-02-04::
   Make SRC able to drive RCS v5.7 (previously needed v5.8 or later).
   If you change nothing in the template commit during edit, commit is canceled.

1.2: 2016-01-29::
   Documentation improvements based on user feedback.

1.1: 2016-01-27::
   Avoid upchucking on status A files if we happen to go through a modify check.
   Add regression test for commit-comment editing.
   Force binary I/O - a step towards Python 3 porting.

1.0: 2016-01-26::
   Now hosted on gitlab.
   Fix for Tom Willemse's multi-file commit bug.

0.19: 2015-04-02::
   A pylint audit caught two unbound variables in odd cases.

0.18: 2014-12-23::
   Reversed range expressons are now supported.
   In list and log, explicit ranges are no longer reversed.

0.17: 2014-12-19::
   Undocumented 'add' command abolished; Emacs VC will use 'commit -a' instead.

0.16: 2014-12-18::
   Allow -- as an argument ender in src diff.
   Changes to a workfile's x bits are propagated to its master on checkin.

0.15: 2014-12-08::
   Deal gracefully with directories passed as arguments.

0.14: 2014-11-29::
   Fixed bugs affecting argument parsing in the presence of numeric filenames.

0.13: 2014-11-24::
   Fixed bug that caused spurious modified status after commit.

0.12: 2014-11-21::
   Log command shows the branch in the header line for each revision.
   List and log command now have an -l option to play nice with Emacs VC.

0.11: 2014-11-19::
   File-not-modified status character changed to '=' to match Mercurial.
   Fixed-width cutlines and list -f option support Emacs VC mode.

0.10: 2014-11-19::
   Modified check is now done by content, not modification date.

0.9: 2014-11-16::
   SRC is now feature-complete as planned.
   Branch deletion is implemented and tested.
   'src rename' now renames tags and branches - no longer an alias for 'move'.
   In tag and branch, a 'create' or '-c' modifier is now required to make tags.

0.8: 2014-11-14::
   A branch label names the tip revision of its branch.
   src log and src list skip ignored and unregistered files in arguments.
   The embedded help has been enriched and now features multiple topics.
   ! works to negate patterns in ignore files.
   src status -a forces status display of all files, including I and ?.

0.7: 2014-11-13::
   Bugfix release: fix initial file commit.  There was a bug in my tests...

0.6: 2014-11-12::
   Useful boilerplate in commit and amend message buffers.
   Tag names for revisions work; so does tag renaming.
   Branch-sensitive traversal with .. is working.
   Fixed yet another fast-export bug.

0.5: 2014-11-11::
   Removed src add.  The first src commit to a file adds it.
   Branching is mostly working - branch delete isn't implemented,
   Tag and branch names to identify revisions don't work yet.
   'U' (unmodified) becomes '.' in src status listings.
   src fast-export was broken in 0.4, it's fixed now.
   Added src version.

0.4: 2014-11-10::
   Improvements to src fast-import.
   src commit now has an -e option to force editing after -m or -f.
   There is now a "src amend".

0.3: 2014-11-09::
   There is now a "src status" command, and .srcignore affects it.
   src fast-import is implemented (by callout to rcs-fast-import).

0.2: 2014-11-08::
   There is a public repository on Gitorious
   All the initially projected commands except branch are implemented.
   The bug that caused failures with vi has been fixed.
   ".." is a legal range separator as well as "-".
   There is a regression-test suite, and more documentation.

0.1: 2014-11-07::
   Initial proof-of-concept release.
